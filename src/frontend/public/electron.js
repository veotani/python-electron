const { app, BrowserWindow } = require('electron');
const isDev = require('electron-is-dev');   
const path = require('path');
 
let mainWindow;

function startServer() {
    const serverPath = isDev ? "server.exe" : path.join(__dirname, '../server.exe')
    return require('child_process').execFile(serverPath, function(err, stdout, stderr) {
        if (err) {
            console.log("Ошибка сервера: ", err);
        }
        if (stdout) {
            console.log("Сообщение сервера: ", stdout)
        }
        if (stderr) {
            console.log("Ошибка сервера: ", stderr);
        }
    });
}
 
function createWindow() {
    const python = startServer();
    mainWindow = new BrowserWindow({
        width:800,
        height:600,
        show: false
    });
    const startURL = isDev ? 'http://localhost:3000' : `file://${path.join(__dirname, '../build/index.html')}`;
    // const startURL = `file://${path.join(__dirname, '../public/index.html')}`;
 
    mainWindow.loadURL(startURL);
 
    mainWindow.once('ready-to-show', () => mainWindow.show());
    mainWindow.on('closed', () => {
        if (python) {
            python.kill();
        }
        mainWindow = null;
    });
}
app.on('ready', createWindow);